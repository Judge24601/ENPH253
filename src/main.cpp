/**
 * Base Libraries (Wire, Arduino, etc)
 * Imported Libraries (OLED, FreeIMU, etc)
 * Our Libraries
 */
#include <Arduino.h>
#include <Wire.h>

#include <SSD1306Ascii.h>
#include <SSD1306AsciiWire.h>

#include <Constants.h>

#include <Helper.h>
#include <Motor.h>
#include <Order66.h>
#include <PID.h>

long lastLoopStart = 0l;
Order66::Master master;

void setup() {
  // timer setup
  disableDebugPorts();
  Helper::timerSetup();
  master.setState(Order66::MasterState::HorizontalPostIR);
}

void loop()
{
  lastLoopStart = millis();
  master.poll();

  // regulate loop time
  Helper::regulateLoop(lastLoopStart, millis());
}
