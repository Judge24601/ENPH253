//This is the main.cpp for the slave Blue Pill
//To use, replace the main.cpp in src with this.

/**
 * Base Libraries (Wire, Arduino, etc)
 * Imported Libraries (OLED, FreeIMU, etc)
 * Our Libraries
 */
#include <Arduino.h>
#include <Wire.h>

#include <SSD1306Ascii.h>
#include <SSD1306AsciiWire.h>

#include <Constants.h>
#include <Gyro.h>
#include <Helper.h>
#include <Motor.h>
#include <Order66.h>
#include <PID.h>

long lastLoopStart = 0l;
Order66::Slave slave;

void setup() {
  Serial.begin(9600);
  slave.setState(SlaveState::RampClimbing)
  // timer setup
  Helper::timerSetup();
}

void loop()
{
  lastLoopStart = millis();
  slave.poll();
  Helper::regulateLoop(lastLoopStart, millis());
}
