#include <Arduino.h>
#include <Servo.h>

#include <BridgeLay.h>
#include <Claw.h>
#include <Constants.h>
#include <IR.h>
#include <Motor.h>
#include <PID.h>

#include <Order66.h>

namespace Order66 {
  MasterState Master::poll () {
    // turn off slave signal every loop
    endSlaveSignal();

    switch(state) {
      case MasterState::Inactive:
        // start button press => start
        if(startButton.checkPinState()) {
          advanceState();
          signalSlaveAdvance();
        }
        break;

      case MasterState::RampClimbing:
        // grab ewok
        if(rightClaw.foundEwok() && ewokCount < 1) {
          stopSlave();
          rightClaw.retrieveEwok();
        }
        // reset claw + advance state
        if(rightClaw.successfulRetrieval()){
          goSlave();
          signalSlaveAdvance();
          rightClaw.reset();
          ewokCount++;
          advanceState();
        }
        break;

      case MasterState::HorizontalPreIR:
        // grab ewok
        if(rightClaw.foundEwok() && ewokCount < 2) {
          stopSlave();
          rightClaw.retrieveEwok();
        }
        // reset claw to up Position to pass gate
        if(rightClaw.successfulRetrieval()) {
          ewokCount++;
          goSlave();

          // move claws to pass gate
          rightClaw.upPosition();
          leftClaw.upPosition();
          leftClaw.orientationServo.write(0); //TEMP: Switch to constant?
          signalSlaveAdvance();
          delay(1000); //Wait for turn to complete

          // choose best QSD to measure IR
          ir.setQSDCode();
          advanceState();
        }
        break;

      case MasterState::IRWaiting:
        if(ir.getSignal() == IRSignal::Go) {
          advanceState();
          stormtrooperStartTime = millis();
          stormtrooperTimer = 0;
          signalSlaveAdvance();
        }
        break;

      case MasterState::HorizontalPostIR:
        // wait until we find the stormtrooper
        if(rightClaw.foundEwok()) {
          ewokCount = 2; //TEMP
          stormtrooperTimer = -1;
          delay(2000);
          leftClaw.reset();
        }

        // look for ewoks
        if(leftClaw.foundEwok() && ewokCount < 3) {
            signalSlaveAdvance();
            delay(50);
            endSlaveSignal();
            leftClaw.retrieveEwok(); //slow not detached
        }
        // reset claw
        if(leftClaw.successfulRetrieval()) {
          // leftClaw.reset();
          ewokCount++;
        }

        if(ewokCount == 3) {
          signalSlaveAdvance();
          // advanceState();
          detachingStartTime = millis();
          detachingTimer = 0;
        }
        break;

      case MasterState::Detaching:
        // Wait until big bot is lined up for detachment
        if(detachingTimer < MasterNS::WAIT_DETACH_TIME_THRESHOLD) {
          detachingTimer = millis() - detachingStartTime;
        }
        else {
          //Complete detachment
          forward(MasterNS::DETACHING_SPEED);
          delay(MasterNS::DETACHING_TIME);
          advanceState();
          detached = true;
          stop();
          turningStartTime = millis();
          turningTimer = 0;
        }
        break;

      case MasterState::InitialTurn:
        if(!turnStarted){
          forward(60);
          delay(150);
          turnStarted = true;
          //temp
          turningTimer = 0;
          turningStartTime = millis();
        }
        if(leftClaw.foundEwok(detached) && ewokCount < 4) {
          turningClawDuration = millis();
          delay(200);
          stop();
          leftClaw.retrieveEwok(detached);
        }
        // reset claw
        if(leftClaw.successfulRetrieval()){
          turningClawDuration = millis() - turningClawDuration;
          leftClaw.reset();
          ewokCount++;
        }
        // Wait for the turn to be complete, regardless of claw time
        if(turningTimer < MasterNS::TURNING_TIME_THRESHOLD) {
          stopped = false;
          leftMotor.speed(MasterNS::LEFT_TURNING_SPEED);
          rightMotor.speed(MasterNS::RIGHT_TURNING_SPEED);
          turningTimer = millis() - turningClawDuration - turningStartTime;
        }
        else {
          //advanceState();
          stop();
          edgeStartTime = millis();
          edgeTimer = 0;
        }
        break;

      case MasterState::EdgeFollowing:
        edgeFollow.usePID();
        // if(edgeTimer < MasterNS::EDGE_TIME_THRESHOLD) { edgeTimer = millis() - edgeStartTime; }
        // else { advanceState(); }
        break;

      case MasterState::Chewbacca:
        if(leftClaw.foundEwok(detached) && ewokCount < 5) {
          stop();
          leftClaw.retrieveEwok(detached);
        }
        // reset claw
        if(leftClaw.successfulRetrieval()){
          leftClaw.reset();
          ewokCount++;
        }

        if(ewokCount < 5) { forward(MasterNS::STANDARD_MOTOR_SPEED); }
        else { advanceState(); }

        break;
      case MasterState::ZiplineDeploy:
        //Lift up and move forward
        lift(HIGH);
        forward(MasterNS::ZIPLINE_APPROACH_SPEED);
        delay(MasterNS::ZIPLINE_APPROACH_TIME);
        //Lower lift and back up to deploy basket
        lift(LOW);
        forward(-MasterNS::ZIPLINE_APPROACH_SPEED);
        delay(MasterNS::ZIPLINE_APPROACH_TIME);
        advanceState();
        break;
      case MasterState::Done:
        stop();
        break;
      default:
        break;
    }

    return state;
  }

  void Master::stop() {
    if(stopped) { return; }
    stopped = true;

    // coast
    int coastTime = 100;
    leftMotor.stop();
    rightMotor.stop();
    delay(coastTime);
  }

  void Master::forward(int speed) {
    leftMotor.speed(speed);
    rightMotor.speed(speed);
  }

  void Master::lift(bool position) {
    if(position == HIGH) {
      digitalWrite(MasterNS::UP_LIFT_PIN, HIGH);
      digitalWrite(MasterNS::DOWN_LIFT_PIN, LOW);
      int hitLimitCount = 0;
      delay(200);
      while(hitLimitCount < 5) {
        if(digitalRead(MasterNS::LIMIT_LIFT_PIN) == HIGH) {
          hitLimitCount++;
        } else {
          hitLimitCount = 0;
        }
        delay(1);
      }
      digitalWrite(MasterNS::UP_LIFT_PIN, LOW);
    } else {
      digitalWrite(MasterNS::DOWN_LIFT_PIN, HIGH);
      digitalWrite(MasterNS::UP_LIFT_PIN, LOW);
      delay(2000);
      digitalWrite(MasterNS::DOWN_LIFT_PIN, LOW);
    }
  }

  bool Master::advanceState() {
    if ( state == MasterState::Done) { return false; }

    state = static_cast<MasterState>(static_cast<int>(state) + 1);
    return true;
  }

  void Master::signalSlaveAdvance() {
    digitalWrite(MasterNS::ADVANCE_SLAVE_PIN, HIGH);
    //Ensure slave has recieved signal to advance
    delay(100);
  }

  void Master::endSlaveSignal() { digitalWrite(MasterNS::ADVANCE_SLAVE_PIN, LOW); }

  void Master::stopSlave() {
    digitalWrite(MasterNS::STOP_SLAVE_PIN, HIGH);
    //Ensure slave has stopped before activating other aspects
    delay(500);
  }

  void Master::goSlave() { digitalWrite(MasterNS::STOP_SLAVE_PIN, LOW); }

}  // namespace Order66

