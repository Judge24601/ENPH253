#ifndef Order66_h
#define Order66_h
#include <Arduino.h>
#include <Servo.h>

#include <BridgeLay.h>
#include <Claw.h>
#include <Constants.h>
#include <Helper.h>
#include <IR.h>
#include <Motor.h>
#include <PID.h>

namespace Order66 {

  /* States */
  enum class MasterState {
    Inactive,
    RampClimbing,
    HorizontalPreIR,
    IRWaiting,
    HorizontalPostIR,
    ZiplineAlign1,
    TowerDeploying,
    Detaching,
    InitialTurn,
    EdgeFollowing,
    Chewbacca,
    ZiplineDeploy,
    Done
  };
  enum class SlaveState {
    Inactive,
    RampClimbing,
    HorizontalPreBridge,
    BridgeLaying,
    BridgeCrossing,
    HorizontalPreIR,
    IRLooking,
    Returning,
    BridgeCrossHome,
    DownRampHome,
    Complete,
    IRWaiting,
    HorizontalPostIR,
    EdgeSearching,
    RampDeploying,
    Done
  };

  /* Bots */
  class Master {
    public:
      Master():
        leftMotor(MasterNS::LEFT_MOTOR_PIN_1, MasterNS::LEFT_MOTOR_PIN_2),
        rightMotor(MasterNS::RIGHT_MOTOR_PIN_1, MasterNS::RIGHT_MOTOR_PIN_2),
        leftClaw(Side::Left),
        rightClaw(Side::Right),
        ewokCount(0),
        turningClawDuration(0l),
        state(MasterState::Inactive),
        edgeFollow(PIDType::EdgeFollower, leftMotor, rightMotor, MasterNS::STANDARD_MOTOR_SPEED), 
        startButton(MasterNS::START_BUTTON_PIN, INPUT_PULLDOWN)
        {
          // set communication pins
          pinMode(MasterNS::ADVANCE_SLAVE_PIN, OUTPUT);
          pinMode(MasterNS::STOP_SLAVE_PIN, OUTPUT);
          digitalWrite(MasterNS::ADVANCE_SLAVE_PIN, LOW);
          digitalWrite(MasterNS::STOP_SLAVE_PIN, LOW);

          // setup lift pins
          pinMode(MasterNS::LIMIT_LIFT_PIN, INPUT_PULLDOWN);
          digitalWrite(MasterNS::UP_LIFT_PIN, LOW);
          digitalWrite(MasterNS::DOWN_LIFT_PIN, LOW);
          pinMode(MasterNS::UP_LIFT_PIN, OUTPUT);
          pinMode(MasterNS::DOWN_LIFT_PIN, OUTPUT);
        }

      /*
      @brief Returns current state of robot and performs action for one loop.
      */
      MasterState poll();

      /**
       * @brief Set the state of slave. ONLY FOR DEBUGGING. Do not use on competition day
       */
      void setState(MasterState state) { this->state = state; }

      void lift(bool position);

    private:
      /*
      @brief Advances the master state

      @returns true if successful, false if unsuccessful (out of states)
      */
      bool advanceState();

      /*
      @brief Advances the slave state

      */
      void signalSlaveAdvance();

      /*
      @brief Ends Advance signal

      */
      void endSlaveSignal();

      void stopSlave();

      void goSlave();

      Motor leftMotor;
      Motor rightMotor;
      Servo scissorLift;
      Claw leftClaw;
      Claw rightClaw;
      PID edgeFollow;

      int ewokCount;
      MasterState state;

      //Timer for bypassing stormtroopers
      int stormtrooperTimer;
      long stormtrooperStartTime;

      //Timer for finishing detachment
      int detachingTimer;
      long detachingStartTime;

      //Timer for turn to edge follow
      int turningTimer;
      long turningClawDuration;
      long turningStartTime;

      //Timer for edge following
      int edgeTimer;
      long edgeStartTime;

      IR ir;
      Debounce startButton;

      bool detached = false;
      bool stopped;
      bool turnStarted = false;
      void stop();
      void turn(int angle);
      void forward(int speed);

  };

  class Slave {
    public:
        Slave():
          leftMotor(SlaveNS::LEFT_MOTOR_PIN_1, SlaveNS::LEFT_MOTOR_PIN_2),
          rightMotor(SlaveNS::RIGHT_MOTOR_PIN_1, SlaveNS::RIGHT_MOTOR_PIN_2),
          tapeFollow(PIDType::TapeFollower, leftMotor, rightMotor, 100, Side::Left),
          bridgeLayer(leftMotor, rightMotor),
          state(SlaveState::Inactive),
          stopped(false),
          doneRamp(false),
          lastAdvanceTime(0l),
          advancePin(SlaveNS::ADVANCE_PIN, INPUT_PULLDOWN),
          stopPin(SlaveNS::STOP_PIN, INPUT_PULLDOWN) {}

        /*
        @brief Returns current state of robot and performs action for one loop.
        */
        SlaveState poll();
        /**
         * @brief Set the state of master. ONLY FOR DEBUGGING. Do not use on competition day
         */
        void setState(SlaveState state) { this->state = state; }

        /**
         * @brief move slave for given time
         */
        void moveForTime(int leftMotorSpeed, int rightMotorSpeed, int moveTime);

    private:
        Motor leftMotor;
        Motor rightMotor;
        PID tapeFollow;
        BridgeLay bridgeLayer;
        bool stopped;
        void stop();
        bool doneRamp;
        void turn(int angle);
        void forward(int speed);
        long lastAdvanceTime;
        SlaveState state;
        Debounce advancePin;
        Debounce stopPin;
        
        // ramp climbing
        long startFastRampTime;
        bool endRamp = false;

        // ir gate turn
        bool irGateTurnComplete = false;
        long passedGateTimer;

        // predetach turn
        bool preDetachTurnComplete = false;
        
        /*
        @brief Advances the big bot state

        @returns true if successful, false if unsuccessful (end of list)
        */
        bool advanceState();
    };
} // namespace Order66

#endif
