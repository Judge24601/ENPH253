#ifndef Claw_h
#define Claw_h
#include <Arduino.h>
#include <Constants.h>
#include <Helper.h>
#include <Servo.h>

#include <Motor.h>

class Claw {

private:
  Side clawSide;
  int sensorPin;
  int detachedSensorPin = -1;
  bool lastEwokRetrieved;
  Servo grabServo;
  long pickupCooldownStart;

public:

  /**
   * Construct the claw sensor pins and default orientation
   **/
  Claw(Side clawSide): clawSide(clawSide), pickupCooldownStart(0L) {
    switch (clawSide) {
      case Side::Left :
        sensorPin = ClawNS::Left::SENSOR_PIN;
        detachedSensorPin = ClawNS::Left::DETACHED_SENSOR_PIN;
        grabServo.attach(ClawNS::Left::OPEN_CLAW_SERVO);
        orientationServo.attach(ClawNS::Left::ORIENT_CLAW_SERVO);
        orientationServo.write(ClawNS::Left::STARTING_ROTATION);
        grabServo.write(ClawNS::Left::ARCHWAY_ANGLE);
        break;
      case Side::Right :
        sensorPin = ClawNS::Right::SENSOR_PIN;
        grabServo.attach(ClawNS::Right::OPEN_CLAW_SERVO);
        orientationServo.attach(ClawNS::Right::ORIENT_CLAW_SERVO);
        // orientationServo.write(ClawNS::Right::STARTING_ROTATION);
        orientationServo.write(ClawNS::Right::UP_ANGLE);
        //grabServo.write(ClawNS::Right::OPEN_ANGLE);
        grabServo.write(ClawNS::Right::ARCHWAY_ANGLE);
        break;
    }
    pinMode(sensorPin, INPUT_PULLUP);
  };

  Servo orientationServo; //TEMP

  void setGrab(bool closed);

  void setOrientation(bool up);

  void setRackPosition(bool position, int ewokMultiplier = 1);

  /**
   * Executes Pickup Cycle
   **/
  void retrieveEwok(bool detached = false, bool slow = false);

  /**
  * Sets Claw up for Pickup Cycle
  **/
  void reset();

  /**
  * Returns whether the claw can see an Ewok
  **/
  bool foundEwok(bool detached = false);

  /**
  * Returns whether the last ewok was retrieved succesfully (expand later)
  **/
  bool successfulRetrieval();

  void upPosition();

};

#endif
