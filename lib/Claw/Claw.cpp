#include <Arduino.h>
#include <Constants.h>
#include <Servo.h>

#include <Motor.h>

#include <Claw.h>

void Claw::retrieveEwok(bool detached, bool slow) {
  if(detached) {
    orientationServo.write(175);
    delay(50);
    orientationServo.write(170);
    delay(50);
    orientationServo.write(165);
    delay(500);
  }
  //Grab Ewok
  if(!slow) {
    setGrab(true);
    delay(1000);
  } else {
    for(int i = ClawNS::Left::OPEN_ANGLE; i <= ClawNS::Left::CLOSED_ANGLE; i+= 5) {
      grabServo.write(i);
      delay(50);
    }
    delay(200);
    orientationServo.write(150);
    delay(50000);
  }

  //Rotate Up
  setOrientation(HIGH);
  delay(600);

  //Drop Ewok
  setGrab(false);
  delay(400);
  lastEwokRetrieved = true;

  //reset timer for next ewok
  pickupCooldownStart = millis();
}

void Claw::reset() {
  setOrientation(LOW);
  setGrab(false);
  //Allow reset to complete
  delay(200);
}

void Claw::upPosition() {
  setOrientation(HIGH);

  switch (clawSide) {
    case Side::Left : grabServo.write(ClawNS::Left::ARCHWAY_ANGLE); break;
    case Side::Right : grabServo.write(ClawNS::Right::ARCHWAY_ANGLE); break;
  }
}

bool Claw::successfulRetrieval() {
  if(lastEwokRetrieved) { lastEwokRetrieved = false; return true; }
  return false;
}

bool Claw::foundEwok(bool detached) {
  long cooldownTime = millis() - pickupCooldownStart;
  // long cooldownTime = 10000; //TEMP
  //Ensure we do not accidentally activate the retrieval
  int pinToRead = sensorPin;
  if(detached) {pinToRead = detachedSensorPin;}
  if (cooldownTime > ClawNS::COOLDOWN_TIME) { return !digitalRead(pinToRead); }
  else { return false; }
}

void Claw::setGrab(bool closed) {
  // close claw
  if(closed) {
    switch (clawSide) {
      case Side::Left : grabServo.write(ClawNS::Left::CLOSED_ANGLE); break;
      case Side::Right : grabServo.write(ClawNS::Right::CLOSED_ANGLE); break;
    }
  }

  // open claw
  else {
    switch (clawSide) {
      case Side::Left : grabServo.write(ClawNS::Left::OPEN_ANGLE); break;
      case Side::Right : grabServo.write(ClawNS::Right::OPEN_ANGLE); break;
    }
  }
}

void Claw::setOrientation(bool up) {
  if(up) {
    switch (clawSide) {
      case Side::Left : orientationServo.write(ClawNS::Left::UP_ANGLE); break;
      case Side::Right : orientationServo.write(ClawNS::Right::UP_ANGLE); break;
    }
  }
  else {
    switch (clawSide) {
      case Side::Left : orientationServo.write(ClawNS::Left::DOWN_ANGLE); break;
      case Side::Right : orientationServo.write(ClawNS::Right::DOWN_ANGLE); break;
    }
  }
}
