//Standard Libraries
#include <SPI.h>
#include <Wire.h>
#include <Arduino.h>

//FreeIMU Libraries
#include <MPU60X0_6Axis_MotionApps20.h>
#include <I2Cdev.h>

//Our libraries
#include <Constants.h>
#include <Gyro.h>

/**
 * A lot of this is shamelessly cribbed from the example Arduino program in the MPU60X0 library.
 * Deviations will be noted and explained thoroughly.
 * Braeden Jury
 * Ewok Saviour #0
 **/

namespace MPU {

  volatile bool Gyro::mpuInterrupt = false;

  Gyro::Gyro ():
    mpu(MPU60X0(true, GyroNS::SPI_ADDRESS)),
    dmpReady(false)
  {
    mpu.initialize();
    mpu.setRate(4); 						// Sample Rate (200Hz = 1Hz Gyro SR / 4+1)
    mpu.setDLPFMode(MPU60X0_DLPF_BW_20);			// Low Pass filter 20hz
    mpu.setFullScaleGyroRange(MPU60X0_GYRO_FS_250);		// 250? / s
    mpu.setFullScaleAccelRange(MPU60X0_ACCEL_FS_2);		// +-2g

    // verify connection
    Serial.println(F("Testing device connections..."));
    Serial.println(mpu.testConnection() ? F("MPU60X0 connection successful") : F("MPU60X0 connection failed"));

    // load and configure the DMP
    Serial.println(F("Initializing DMP..."));
    devStatus = mpu.dmpInitialize();

    // make sure it worked (returns 0 if so)
    if (devStatus == 0) {
        // turn on the DMP, now that it's ready
        Serial.println(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        Serial.println(F("Enabling interrupt detection..."));
        attachInterrupt(GyroNS::GYRO_INTERRUPT_PIN, reinterpret_cast<void (*)()>(&dmpDataReady), RISING);
        mpuIntStatus = mpu.getIntStatus();

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        Serial.println(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;

        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
    }
  }
  void Gyro::dmpDataReady() {
    mpuInterrupt = true;
  }

  Data Gyro::poll() {
    // if programming failed, don't try to do anything
    if (dmpReady && (mpuInterrupt || fifoCount >= packetSize)) {
      Gyro::updateData();
    }
    return data;
  }

  void Gyro::updateData () {
    // reset interrupt flag and get INT_STATUS byte
    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    // get current FIFO count
    fifoCount = mpu.getFIFOCount();

    // check for overflow (this should never happen unless our code is too inefficient)
    if ((mpuIntStatus & MPU60X0_INTERRUPT_FIFO_OFLOW) || fifoCount == 1024) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & MPU60X0_INTERRUPT_DMP_INT) {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);

        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;

        // save Euler angles in degrees
        mpu.dmpGetGravity(&gravity, &quarternion);
        mpu.dmpGetYawPitchRoll(data.angles, &quarternion, &gravity);

        // save initial world-frame acceleration, adjusted to remove gravity
        // and rotated based on known orientation from quaternion
        mpu.dmpGetQuaternion(&quarternion, fifoBuffer);
        mpu.dmpGetAccel(&raw_accels, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &quarternion);
        mpu.dmpGetLinearAccelInWorld(&(data.acceleration), &real_accels, &quarternion);
    }
  }
}
