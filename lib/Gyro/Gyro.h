#ifndef Gyro_h
#define Gyro_h

//Standard Libraries
#include <SPI.h>
#include <Wire.h>
#include <Arduino.h>

//FreeIMU Libraries
#include <MPU60X0_6Axis_MotionApps20.h>
#include <I2Cdev.h>

//Our libraries
#include <Constants.h>

namespace MPU {
  struct Data {
    float angles[3];         // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector
    VectorInt16 acceleration;    // [x, y, z]            world-frame accel sensor measurements
  };
  class Gyro {
  public:
    Gyro();
    static volatile bool mpuInterrupt; //Detects whether MPU Interrupt Pin is high
    struct Data data;
    Data poll(); //To be called constantly
  private:
    MPU60X0 mpu;
    bool dmpReady;  // set true if DMP init was successful
    uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
    uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
    uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
    uint16_t fifoCount;     // count of all bytes currently in FIFO
    uint8_t fifoBuffer[64]; // FIFO storage buffer

    //Base Measurements
    Quaternion quarternion;           // [w, x, y, z]         quaternion container
    VectorInt16 raw_accels;         // [x, y, z]            accel sensor measurements
    VectorInt16 real_accels;     // [x, y, z]            gravity-free accel sensor measurements
    VectorFloat gravity;    // [x, y, z]            gravity vector

    static void dmpDataReady();
    void updateData(); //The important method
  };

}

#endif
