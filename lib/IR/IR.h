#ifndef IR_h
#define IR_h
#include <Arduino.h>

#include <Constants.h>
#include <Helper.h>

enum class IRSignal {
  Stop,
  Go,
  NoSignal
};

class IR {

public:

  /**
   * IR Constructor
   **/
  IR():
    wasPreviouslyOnekHz(false),
    onekHzSignal(0),
    tenkHzSignal(0),
    lastTenkHzSignal(4096) {
      pinMode(IRNS::ONE_KHZ_PIN, INPUT_ANALOG);
      pinMode(IRNS::TEN_KHZ_PIN, INPUT_ANALOG);
      pinMode(IRNS::CONTROL_PIN_1, OUTPUT_OPEN_DRAIN);
      pinMode(IRNS::CONTROL_PIN_2, OUTPUT_OPEN_DRAIN);
    };

  /**
   * Returns Stop if the gate reads 1kHz or was not just 1kHz and is now 10
   * Returns Go if the gate reads 10kHz right after reading 1kHz
   * Returns NoSignal if the IR needs to be refound
   * Sets onekHzSignal and tenkHzSignal
   **/
  IRSignal getSignal();

  /* Getter methods */
  int getOnekHzSignal() { return onekHzSignal; }
  int getTenkHzSignal() { return tenkHzSignal; }

  void setQSDCode();

private:

  bool aboveThreshold(int sensorValue, int threshold);
  bool wasPreviouslyOnekHz;
  int onekHzSignal;
  int tenkHzSignal;
  int lastTenkHzSignal;
  int QSDCode[2] {0, 0};
};


#endif
