#include <Arduino.h>

#include <Constants.h>
#include <Helper.h>

#include <IR.h>

IRSignal IR::getSignal() {
  // set multiplexer code
  digitalWrite(IRNS::CONTROL_PIN_1, QSDCode[0]);
  digitalWrite(IRNS::CONTROL_PIN_2, QSDCode[1]);

  // get average IR values
  tenkHzSignal = Helper::getAverageAnalogValue(IRNS::TEN_KHZ_PIN, IRNS::NUM_READINGS);
  onekHzSignal = Helper::getAverageAnalogValue(IRNS::ONE_KHZ_PIN, IRNS::NUM_READINGS);
  
  // print values
  Serial.print("10 kHz: "); Serial.println(tenkHzSignal);
  Serial.print("1 kHz: "); Serial.println(onekHzSignal);
  // compare to threshold
  bool timeToGo = (tenkHzSignal - lastTenkHzSignal > IRNS::TEN_DIFF_THRESHOLD);

  if(timeToGo) { return IRSignal::Go; }
  lastTenkHzSignal = tenkHzSignal;
  return IRSignal::Stop;
}

void IR::setQSDCode() {
  int multiplexerCode1;
  int multiplexerCode2;
  int testSignal = 0;
  int compareSignal = 0;

  // try all 00, 01, 10, 11 for multiplexer for best QSD
  for(int i = 0; i < 4; i++) {
    // set multiplexer
    multiplexerCode1 = i % 2;
    multiplexerCode2 = (i / 2) % 2;
    digitalWrite(IRNS::CONTROL_PIN_1, multiplexerCode1);
    digitalWrite(IRNS::CONTROL_PIN_2, multiplexerCode2);

    // get signal
    for(int x = 0; x < IRNS::NUM_QSD_AVERAGE; x++) {
      compareSignal += Helper::getAverageAnalogValue(IRNS::TEN_KHZ_PIN, IRNS::NUM_READINGS);
    }
    compareSignal /= IRNS::NUM_QSD_AVERAGE;

    // compare signal
    if(compareSignal > testSignal) {
      testSignal = compareSignal;
      QSDCode[0] = multiplexerCode1;
      QSDCode[1] = multiplexerCode2;
    }
  }
}
bool IR::aboveThreshold(int sensorValue, int threshold) { return sensorValue > threshold; }
