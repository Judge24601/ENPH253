#include <Arduino.h>
#include <Constants.h>
#include <Helper.h>

int Helper::getAverageAnalogValue(int analogPin, int numReadings) {
  int sensorValue = 0;
  for (int k = 0; k < numReadings; k++) sensorValue += analogRead(analogPin);
  return sensorValue / numReadings;
}

void Helper::fastAnalogWrite(uint8 pin, int duty_cycle8) {
  pwmWrite(pin, duty_cycle8 * MAP_8_BIT_TO_16_BIT);
}

void Helper::timerSetup() {
  for (int i = 1; i <= NUM_TIMERS; i++) {
      HardwareTimer timer(i);
      timer.pause();
      timer.setPeriod(TIMER_PERIOD);
      timer.refresh();
      timer.resume();
  }
}

void Helper::regulateLoop(long startTime, long endTime) {
  int loopTime = endTime - startTime;
  if (loopTime < LOOP_LENGTH) {
    int waitTime = LOOP_LENGTH - loopTime;
    delay(waitTime);
  }
}

bool Debounce::checkPinState() {
  // read pin
  int reading = digitalRead(pin);

  // restart timer if it changed
  if (reading != lastReading) { lastDebounceTime = millis(); }

  // reset lastReading
  lastReading = reading;

  // if it has been at a NEW state for enough time, then update State, else return what it was
  int changeTime = millis() - lastDebounceTime;
  if (changeTime > debounceDelay) { pinState = reading; }

  return pinState;
}
