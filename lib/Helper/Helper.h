#ifndef Helper_H
#define Helper_H

enum class Side {
  Left,
  Right
};

class Debounce {
  private:
    int pin;
    bool pinState;
    bool lastReading;
    long lastDebounceTime;
    int debounceDelay;

  public:

    /**
     * @brief Constructor for Debounce
     * 
     * @param pin digital Pin to read from
     * 
     * @param inputType for pin (INPUT_PULLDOWN, INPUT, INPUT_PULLUP)
     */
    Debounce(int pin, WiringPinMode inputType) :
      pin(pin),
      debounceDelay(DEBOUNCE_DELAY)
      { 
        pinMode(pin, inputType); 
        switch (inputType) {
          case INPUT_PULLUP : pinState = HIGH; break;
          case INPUT_PULLDOWN : pinState = LOW; break;
          default : pinState = LOW; break;  // guess LOW, would be floating on start before first read
        }
        lastReading = pinState;
      }

    /**
     * @brief Non-blocking pin check. 
     * 
     * @return bool of PinState, requires the pin read the same value for a certain time before changing state 
     */
    bool checkPinState();
};

class Helper {
  public:

    /**
     * @brief Get average sensor readings value from analogPin. Average over numReadings samples
     *
     * @param analogPin analog pin number for reading
     * @param numReadings number of readings for average (must be >0)
     *
     * @return int which is the average sensor value
     */
    static int getAverageAnalogValue(int analogPin, int numReadings);

    /**
     * @brief A faster implementation of analogWrite.
     * 
     * @param pin A physical pin on the Blue Pill. Pin mode must be set to PWM on this
     *          pin before this method is called.
     * @param duty_cycle8 An 8-bit duty cycle (ex. 0 to 255 inclusive)
     */ 
    static void fastAnalogWrite(uint8 pin, int duty_cycle8);

    /** 
     * @brief Sets all the timers to 20ms period. Ensures consistency between servos and motors on the same timer.
     */
    static void timerSetup();

    /**
     * @brief Regulate main loop time. Ensures that loopTime >=  Constants::LOOP_LENGTH)
     *
     * @param startTime the start time of the loop (from millis())
     *
     * @param endTime the end time of the loop (from millis())
     */
    static void regulateLoop(long startTime, long endTime);
};

#endif
