#ifndef BridgeLay_h
#define BridgeLay_h

#include <Constants.h>
#include <Helper.h>
#include <Motor.h>
#include <Servo.h>

class BridgeLay {
public:

  /**
   * Constructor
   */
  BridgeLay(Motor leftMotor, Motor rightMotor);

  /**
   * @brief lay bridge. Puts servos both in bridge release angle, then puts them back
   */
  void layBridge();
  
  /**
   * @brief deploys ramp, ready for detachment
   */
  void deployRamp();

  /** 
   * @brief Align with edge. Stops robot when complete
   */
  void edgeAlign();

  /**
   * @brief Updates edge states. Return true if both sensors are on edge, else false
   *
   * @return true if both sensors on edge, else false
   */
  bool bothOnEdge();

  /**
   * @brief Updates edge states. Return true if either sensor is on edge, else false
   *
   * @return bool true if either sensors on edge, else false
   */
  bool onEdge();

  /**
   * @brief checks of sensor is off ramp. 
   * 
   * @return true if left sensor has been above threshold for a consecutive number of loops, else false
   */
  bool sensorOffRamp();
  
  /* getters */
  /**
   * update sensors and return the value 
   */
  int getLeftSensor() { leftSensor = Helper::getAverageAnalogValue(BridgeLayNS::Left::SENSOR_PIN, BridgeLayNS::NUM_READINGS); return leftSensor; }
  int getRightSensor() { rightSensor = Helper::getAverageAnalogValue(BridgeLayNS::Right::SENSOR_PIN, BridgeLayNS::NUM_READINGS); return rightSensor; }

  int getLeftMotorSpeed() { return leftMotorSpeed; }
  int getRightMotorSpeed() { return rightMotorSpeed; }

private:

  // Motors + servos
  Motor leftMotor;
  Motor rightMotor;
  Servo leftServo;
  Servo rightServo;

  // Sensors
  int leftSensor;
  int rightSensor;
  int leftMotorSpeed;
  int rightMotorSpeed;

  // Edge State
  bool leftOnEdge;
  bool rightOnEdge;

  int numTimesOffRamp;
};

#endif
