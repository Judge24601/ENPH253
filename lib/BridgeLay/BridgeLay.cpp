#include <Constants.h>
#include <Helper.h>

#include <BridgeLay.h>

BridgeLay::BridgeLay(Motor leftMotor, Motor rightMotor) :
  leftMotor(leftMotor),
  rightMotor(rightMotor),
  leftOnEdge(false),
  rightOnEdge(false),
  numTimesOffRamp(0)
  {
    // Servo setup and initial angle
    leftServo.attach(BridgeLayNS::Left::SERVO_PIN);
    rightServo.attach(BridgeLayNS::Right::SERVO_PIN);
    leftServo.write(BridgeLayNS::Left::HOLD_BRIDGE_ANGLE);
    rightServo.write(BridgeLayNS::Right::HOLD_BRIDGE_ANGLE);

    // setup sensor pins
    pinMode(BridgeLayNS::Left::SENSOR_PIN, INPUT_PULLUP);
    pinMode(BridgeLayNS::Right::SENSOR_PIN, INPUT_PULLUP);

    // setup ramp deploy
    pinMode(BridgeLayNS::RAMP_DEPLOY_PIN, OUTPUT);
    digitalWrite(BridgeLayNS::RAMP_DEPLOY_PIN, LOW);
  }

bool BridgeLay::bothOnEdge() {
  leftOnEdge = getLeftSensor() > BridgeLayNS::EDGE_THRESHOLD;
  rightOnEdge = getRightSensor() > BridgeLayNS::EDGE_THRESHOLD;

  return (leftOnEdge && rightOnEdge);
}

bool BridgeLay::onEdge() {
  leftOnEdge = getLeftSensor() > BridgeLayNS::EDGE_THRESHOLD;
  rightOnEdge = getRightSensor() > BridgeLayNS::EDGE_THRESHOLD;

  return (leftOnEdge || rightOnEdge);
}

void BridgeLay::layBridge() {
  // wait till fully stopped
  delay(1000);

  // lay bridge (maybe need to lower slowly)
  leftServo.write(BridgeLayNS::Left::RELEASE_BRIDGE_ANGLE);
  rightServo.write(BridgeLayNS::Right::RELEASE_BRIDGE_ANGLE);

  // wait till bridge is fully laid
  delay(2000);

  // return servos to holding position (to fit through gate)
  leftServo.write(BridgeLayNS::Left::RETRACT_ANGLE);
  rightServo.write(BridgeLayNS::Right::RETRACT_ANGLE);
}

void BridgeLay::deployRamp() {
  // turn motor for given time
  digitalWrite(BridgeLayNS::RAMP_DEPLOY_PIN, HIGH);
  delay(BridgeLayNS::RAMP_DEPLOY_TIME);
  digitalWrite(BridgeLayNS::RAMP_DEPLOY_PIN, LOW);
}

void BridgeLay::edgeAlign() {
  while(!bothOnEdge()) {
    // left on edge => turn right wheel more
    if (leftOnEdge) {
      leftMotor.speed(-BridgeLayNS::EDGE_ALIGN_SPEED); 
      rightMotor.speed(BridgeLayNS::EDGE_ALIGN_SPEED);
    }

    // right on edge => turn left wheel more
    else if (rightOnEdge) {
      leftMotor.speed(BridgeLayNS::EDGE_ALIGN_SPEED); 
      rightMotor.speed(-BridgeLayNS::EDGE_ALIGN_SPEED);
    }

    // if neither on edge, then go straight till it finds edge
    else { 
      leftMotor.speed(BridgeLayNS::EDGE_ALIGN_SPEED); 
      rightMotor.speed(BridgeLayNS::EDGE_ALIGN_SPEED);
    }
  }

  // when both on edge, stop
  leftMotor.stop();
  rightMotor.stop();
}

bool BridgeLay::sensorOffRamp() {
  if (getLeftSensor() > BridgeLayNS::OFF_RAMP_THRESHOLD) { numTimesOffRamp++; }
  else { numTimesOffRamp = 0; }

  return numTimesOffRamp >= BridgeLayNS::NUM_TIMES_OFF_RAMP;
}
