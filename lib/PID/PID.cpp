#include <Arduino.h>
#include <Constants.h>
#include <Helper.h>
#include <Motor.h>
#include <PID.h>

PID::PID(PIDType pidType, Motor leftMotor, Motor rightMotor, int motorSpeed, Side lostTurningDirection) :
    pidType(pidType),
    lastError(0),
    summedError(0),
    lastDifferentError(0),
    lastTime(0),
    timeOfLastChange(0),
    leftMotor(leftMotor),
    rightMotor(rightMotor),
    motorSpeed(motorSpeed),
    lostTurningDirection(lostTurningDirection)
    {
      // set member variables based on PIDType
      switch (pidType) {
        case PIDType::TapeFollower : {
          leftSensorPin = TapeFollowerNS::LEFT_SENSOR_PIN;
          rightSensorPin = TapeFollowerNS::RIGHT_SENSOR_PIN;
          summedErrorLimit = TapeFollowerNS::SUMMED_ERROR_LIMIT;
          threshold = TapeFollowerNS::WHITE_THRESHOLD;
          numReadings = TapeFollowerNS::NUM_READINGS;
          KP = TapeFollowerNS::KP;
          KD = TapeFollowerNS::KD;
          KI = TapeFollowerNS::KI;
          break;
        }
        case PIDType::EdgeFollower : {
          leftSensorPin = EdgeFollowerNS::LEFT_SENSOR_PIN;
          rightSensorPin = EdgeFollowerNS::RIGHT_SENSOR_PIN;
          summedErrorLimit = EdgeFollowerNS::SUMMED_ERROR_LIMIT;
          threshold = EdgeFollowerNS::EDGE_THRESHOLD;
          numReadings = EdgeFollowerNS::NUM_READINGS;
          KP = EdgeFollowerNS::KP;
          KD = EdgeFollowerNS::KD;
          KI = EdgeFollowerNS::KI;
          break;
        }
        default : {
          // ERROR never should be here
        }
    }
    // Setup pins. Motor pins are handled by library
    pinMode(leftSensorPin, INPUT_PULLUP);
    pinMode(rightSensorPin, INPUT_PULLUP);
}

void PID::usePID() {
  // TODO: time the loop. Set maximum loop speed
  // TODO: after time trials make it return so we know to stop master

  // Get average QRD values
  getLeftSensorVal();
  getRightSensorVal();

  // print qrd values
  Serial.print("TL: "); Serial.print(leftSensor); Serial.print(" TR: "); Serial.println(rightSensor);
  
  int error;

  // set error based on PIDType
  switch (pidType) {
    case PIDType::TapeFollower : {
      bool leftOnWhite = sensorOnWhite(leftSensor, threshold);
      bool rightOnWhite = sensorOnWhite(rightSensor, threshold);
      error = getTapeError(leftOnWhite, rightOnWhite, TapeFollowerNS::ONE_OFF_ERROR, TapeFollowerNS::BOTH_OFF_ERROR);
      break;
    }

    case PIDType::EdgeFollower : {
      bool leftOnPlatform = !sensorOnEdge(leftSensor, threshold);
      bool rightOffPlatform = sensorOnEdge(rightSensor, threshold);
      error = getEdgeError(leftOnPlatform, rightOffPlatform, EdgeFollowerNS::ONE_OFF_ERROR);
      break;
    }

    default : {
      error = 1000;
      // THROW EXCEPTION, should always have a PID Type
    }
  }

  // calculate summed and derivative error
  summedError = getSummedError(error, summedError, summedErrorLimit);

  double derivativeError;
  // if error just changed, then calculate using most recent time + update time of most recent change
  if (lastError != error) {
    derivativeError = (error - lastError) / (micros() - lastTime);
    lastDifferentError = error;
    timeOfLastChange = lastTime;
  }
  // otherwise, use time of last change
  else derivativeError = (error - lastError) / (micros() - timeOfLastChange);

  // reset last values
  lastError = error;
  lastTime = micros();

  // set new motor speeds
  double adjustment = (KP * error) + (KI * summedError) + (KD * derivativeError);
  leftMotorSpeed = motorSpeed - adjustment;
  rightMotorSpeed = motorSpeed + adjustment;
  Serial.print("L"); Serial.println(leftMotorSpeed);
  Serial.print("R"); Serial.println(rightMotorSpeed);
  leftMotor.speed(leftMotorSpeed);
  rightMotor.speed(rightMotorSpeed);
}

bool PID::sensorOnWhite(int sensorValue, int threshold) {
  if (sensorValue < threshold) { return true; }
  return false;
}

bool PID::sensorOnEdge(int sensorValue, int edgeThreshold) {
  if (sensorValue > edgeThreshold) { return true; }
  return false;
}

int PID::getTapeError(bool leftOnWhite, bool rightOnWhite, int oneOffError, int bothOffError) {
  // ensure correct input
  if (pidType != PIDType::TapeFollower) {}  // THROW EXCEPTION

  int error = lastError;

  // if both sensors are on white, set error based on lastError
  if (leftOnWhite && rightOnWhite) {
    if (lastError > 0) { error = bothOffError; }
    else if (lastError < 0) { error = -bothOffError; }

    // if suddenly went off tape)
    else {
      switch(lostTurningDirection) {
        case Side::Left :
          error = bothOffError;
          break;
        case Side::Right :
          error = -bothOffError;
          break;
      }
    }
  }

  // set error left or right
  else if (rightOnWhite) { error = oneOffError; }
  else if (leftOnWhite) { error = -oneOffError; }

  else { error = 0; }

  return error;
}

int PID::getEdgeError(bool correctLeftPosition, bool correctRightPosition, int oneOffError) {
  // ensure correct input
  if (pidType != PIDType::EdgeFollower){}  // THROW EXCEPTION

  int error = 0;

  // set error left or right
  if (!correctRightPosition) { error = -oneOffError; }
  else if (!correctLeftPosition) { error = oneOffError; }
  else { error = 0; }

  return error;
}

int PID::getSummedError(int error, int lastSummedError, int summedErrorLimit) {
  // get absolute value of summedErrorLimi
  if (summedErrorLimit < 0) { summedErrorLimit = -summedErrorLimit; }

  int summedError = lastSummedError + error;

  // anti-windup
  if (summedError > summedErrorLimit) { summedError = summedErrorLimit; }
  else if (summedError < -summedErrorLimit) { summedError = -summedErrorLimit; }

  return summedError;
}

void PID::reset() {
  lastError = 0;
  summedError = 0;
  lastTime = millis();
  timeOfLastChange = lastTime;
}

bool PID::refindTape(int leftMotorSpeed, int rightMotorSpeed, int maxTime) {
  long startTime = millis();
  bool onTape = true;

  while (bothOnWhite(getLeftSensorVal(), getRightSensorVal())) {
    if (millis() - startTime < maxTime) { 
      leftMotor.speed(leftMotorSpeed);
      rightMotor.speed(rightMotorSpeed);
    }
    else { onTape = false; break; }
  }
  
  leftMotor.stop();
  rightMotor.stop();
  return onTape;
}

bool PID::bothOnWhite(int leftSensor, int rightSensor) {
  bool leftOnWhite = sensorOnWhite(leftSensor, TapeFollowerNS::WHITE_THRESHOLD);
  bool rightOnWhite = sensorOnWhite(rightSensor, TapeFollowerNS::WHITE_THRESHOLD);

  return (leftOnWhite || rightOnWhite);
}
