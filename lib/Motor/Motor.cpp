#include <Arduino.h>
#include <Constants.h>
#include <Motor.h>

Motor::Motor(int pin1, int pin2) : pin1(pin1), pin2(pin2) {
  // No need for pinMode since analogWrite() incorporates it
  analogWrite(pin1, 0);
  pinMode(pin1, OUTPUT);
  delay(20);
  analogWrite(pin2, 0);
  pinMode(pin2, OUTPUT);

  pinMode(pin1, PWM);
  pinMode(pin2, PWM);
}

void Motor::stop() {
  pwmWrite(pin1, 0);
  pwmWrite(pin2, 0);
}

void Motor::speed(int speed) {
  // Limits
  if (speed > 255) speed = 255;
  if (speed < -255) speed = -255;

  // delay if sudden motor speed change
  int difference = abs(speed - previousSpeed);
  if (difference > MOTOR_SPEED_CHANGE_THRESHOLD) { 
    pwmWrite(pin1, 0);
    pwmWrite(pin2, 0);
    delay(MOTOR_SPEED_CHANGE_DELAY); 
  }

  // Write the speeds to the motors
  if (speed > 0) {
    pwmWrite(pin2, 0);
    pwmWrite(pin1, speed * MAP_8_BIT_TO_16_BIT);
  } else {
    pwmWrite(pin1, 0);
    pwmWrite(pin2, abs(speed) * MAP_8_BIT_TO_16_BIT);
  }

  previousSpeed = speed;
}
